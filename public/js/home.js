$(document).ready(function(
    $("ul.nav").on("click","li", function(e){
        e.preventDefault();
        var id=$(this).find("input").val();
        $("#projectSettings").show();
        $("#welcomeMessage").hide();

        var request = $.ajax({
            type: 'GET',
            url: 'getQuery.php',
            data: { "type": "projects", "id": id },
            dataType: "json"})
            .done(function(data) {
                $("#id").val(data.id);
                $("#projectTitle").val(data.Name);
                $("#projectAuthor").val(data.Author);
                $("#projectDescription").val(data.Description);
            })
            .fail(function(e) { alert("ajax error"); })
    });

    $('ul.nav button').click(function(e){
        $("#id").val("");
        $("#projectTitle").val("");
        $("#projectAuthor").val("");
        $("#projectDescription").val("");
        $("#projectSettings").show();
        $("#welcomeMessage").hide();
    });
});
