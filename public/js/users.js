function animateToEditView(user) {
    updateContentEditDiv(user);

    $('#usersDiv').fadeOut('fast', function () {
        $('#editDiv').fadeIn('fast');
    });
}

function animateToTableView() {
    updateUsersTableDiv();

    $('#editDiv').fadeOut('fast', function () {
        $('#usersDiv').fadeIn('fast');
    });
}

function updateUsersTableDiv() {
    var result = "";

    $.get("./users/list", function (users) {
        for (var index in users) {
            result += '<tr><td><input type="checkbox" class="checkbox text-center"></td><td><input type="hidden" value="' + users[index].id + '">' + users[index].name + '</td>';
            result += '<td>' + users[index].age + '</td><td>' + users[index].occupation + '</td></tr>';
        }
    }).fail(function (e) {
        result = "error obtaining items";
    }).always(function () {
        $('#userTable > tbody:last').html(result);
        showTableElementsIfNeeded();
    });
}

function updateContentEditDiv(user) {
    $('#userId').val(user.id);
    $('#userName').val(user.name);
    $('#userAge').val(user.age);
    $('#userOccupation').val(user.occupation);
}

function showTableElementsIfNeeded() {
    if ($('#userTable > tbody:last').has('tr').length > 0) {
        $("#usersDiv h4").html("Detected participants");
        $('#userDetailsForm').show();
        $('#btnRemoveUser').show();

    } else {
        $("#usersDiv h4").html("You have no users! Please add some here");
        $('#userDetailsForm').hide();
        $('#btnRemoveUser').hide();
    }
}

$(document).ready(function () {
    showTableElementsIfNeeded();

    $('#userTable tbody').on("click", "tr", function (e) {
        if (e.target.type == "checkbox") {
            // stop the bubbling to prevent firing the row's click event
            e.stopPropagation();
        } else {
            var id = $("input[type='hidden']", this).val();
            $.ajax({
                type: 'GET',
                url: './users/' + id,
                dataType: "json"
            }).done(function (user) {
                animateToEditView(user);
            }).fail(function () {
                alert("ajax error");
            })
        }
    });

    $('#btnNewUser').click(function () {
        var newUser = {
            id: '',
            name: '',
            age: '',
            occupation: ''
        };

        animateToEditView(newUser);
    });

    $('#btnSaveUser').click(function (e) {
        e.preventDefault();
        var savedUser = {
            name: $('#userName').val(),
            age: $('#userAge').val(),
            occupation: $('#userOccupation').val()
        };
        var id = $('#userId').val();
        var append = '';
        if (id != '') append = '/' + id;
        $.ajax({
            url: './users' + append,
            type: 'POST',
            data: savedUser
        }).done(function (e) {
            animateToTableView();
        }).fail(function (e) {
            alert(e.responseText);
        })
    });

    $('#btnCancelUser').click(function (e) {
        e.preventDefault();
        animateToTableView();
    });

    $('#btnRemoveUser').click(function (e) {
        var selectedUsers = $('#userDetailsForm tr').has(':checkbox:checked').find('input[type="hidden"]').map(function () {
            return $(this).val();
        }).get();

        $.ajax({
            url: './users/delete',
            type: 'POST',
            data: {users: selectedUsers}
        }).done(function (e) {
            updateUsersTableDiv();
        }).fail(function (e) {
            alert(e);
        })
    });
});