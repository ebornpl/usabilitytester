/**
 * Created with IntelliJ IDEA.
 * User: eborn
 * Date: 27.05.2013
 * Time: 11:45
 * To change this template use File | Settings | File Templates.
 */

var runningInterval;
var oTable;

function animateToSessionView() {
    $('#log').fadeOut('fast', function () {
        $('#taskList').fadeIn('fast');
    });
}

function animateToLogView() {
    $('#taskList').fadeOut('fast', function () {
        $('#log').fadeIn('fast');
    });
}

$(document).ready(function () {
    $("#sessionsList").show();
    var sessionIndex=getURLParameter("sessionIndex");
    $("#testExecutionDetails h2").text("Session "+sessionIndex+" - "+users[sessionIndex]);
    $("#sessionsList li:eq("+sessionIndex+")").addClass("active");

    $('.switch').on("switch-change",function(){
        $('.switch').not(this).each(function(){
            $(this).bootstrapSwitch('setState', false, true);
            clearInterval(runningInterval);
            $(this).siblings("h4.counter").html("");
        });
        var start=new Date();
        var counter=$(this).siblings("h4.counter");
        runningInterval=setInterval(function () {
            var elapsed = Math.floor((new Date() - start)/1000);
            elapsed="Elapsed: "+elapsed;
            counter.html(elapsed);
        }, 1000);
    })

    oTable=$('#log table').dataTable({
        "bFilter": false,
        "sScrollY": "200px",
        "bPaginate": false,
        "bScrollCollapse": true,
        "bInfo":false
    });

    $("#log tbody tr").click( function( e ) {
        if ( $(this).hasClass('row_selected') ) {
            $(this).removeClass('row_selected');
        }
        else {
            oTable.$('tr.row_selected').removeClass('row_selected');
            $(this).addClass('row_selected');
        }
    });

    $("#taskList .btn-small").click(function(){
        animateToLogView();
    });

    $("#log .btn-inverse").click(function(){
        animateToSessionView();
    });
})