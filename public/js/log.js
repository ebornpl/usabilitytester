var filters=new Array();
var oTable;
var newestAdded;

$(document).ready(function() {
    oTable=$('#logmessagesTable').dataTable({
        "aaSorting": [[ 2, "desc" ]],
        "sScrollY": "500px",
            "bPaginate": false
    });

    $("#commentTextBox").focus();

    var initialState = oTable.$("td:nth-child(2)").map(function(){
        return $(this).text();
    }).get();
    updateFiltersList(initialState);

    newestAdded=oTable.$("tr").find("td:last").map(function() {return $(this).text();}).get().sort().pop();

    $.fn.dataTableExt.afnFiltering.push(
        function( oSettings, aData, iDataIndex ) {
            var selectedFilters=$('#filters label:has(:checked)').map(function() {return $(this).text();}).get();
            var sensor = aData[1];
            return $.inArray(sensor,selectedFilters)>=0;
        }
    );

    $("#filters").on("click", "label", function(){
        oTable.fnDraw();
    });

    $("#logmessagesTable").on('click','tbody tr', function(){
        if ( $(this).hasClass('row_selected') ) {
            $(this).removeClass('row_selected');
        }
        else {
            oTable.$("tr").removeClass('row_selected');
            $(this).addClass('row_selected');
        }
    });

    $("#commentTextBox").keypress(function(e) {
        if(oTable.$("tr.row_selected").length==0){
            $("#logmessagesTable tbody>tr:first").addClass('row_selected');
        }

        if(e.which == 13) {
             postComment();
        }
    });

    $("#commentButton").click(function(e){
        postComment();
    });

    $("#filtersToggleButton").click(function(e){
        if($('#filters input:not(:checked)[type="checkbox"]').length==0){
            $('#filters input:checkbox').prop('checked',false);
        } else {
            $('#filters input:checkbox').prop('checked',true);
        }
        oTable.fnDraw();
    });

    queryNewData();

} );

function updateFiltersList(arrayOfSensors){
    //leave only the new ones
    arrayOfSensors=$.grep(arrayOfSensors,function(el, index){
        return $.inArray(el,filters)<0 && index == $.inArray(el,arrayOfSensors);
    });

    $.each(arrayOfSensors,function(index, value){
        $('#filters').append('<label class="checkbox"><input type="checkbox" value="" checked="true">'+value+'</label>');
        filters.push(value);
    });
}

function postComment(){
    if($("#commentTextBox").val()!=""){
        var selectedTime=oTable.$("tr.row_selected").find("td:last").text();

        var newComment = {
            message: $("#commentTextBox").val(),
            time: selectedTime,
            sensor_type: 'Comment',
            tasksession_id: $('#taskSessionId').val()
        };

        $.ajax({
            url: '../logmessages/comment',
            type: 'POST',
            data: newComment
        }).done(function () {
                var scrollPos=$(".dataTables_scrollBody").scrollTop();
                var rowIndex=oTable.fnAddData(
                    [$("#commentTextBox").val(), "Comment", selectedTime]
                );

                var nTr = oTable.fnSettings().aoData[ rowIndex[0] ].nTr;
                $(nTr).addClass('sensor_Comment');

                updateFiltersList(["Comment"]);
                $(".dataTables_scrollBody").scrollTop(scrollPos);
                $("#commentTextBox").val("");
                $("#commentTextBox").focus();
                oTable.$("tr").removeClass('row_selected');
            }).fail(function (e) {
                alert(e.responseText());
            });
    }
}

function queryNewData(){
    $.ajax({
        url: './recent',
        type: 'GET',
        data: {
            time: newestAdded, tasksession_id: $('#taskSessionId').val()
        }
    }).done(function (logs) {
        if(logs.length>0){
            var mappedLogs=$.map(logs, function(o) { return [[o["message"],o["sensor_type"],o["time"]]]; }) ;
            var scrollPos=$(".dataTables_scrollBody").scrollTop();
            var indexes=oTable.fnAddData(mappedLogs);
            $(".dataTables_scrollBody").scrollTop(scrollPos);

            var sensors=$.map(logs, function(o) { return o["sensor_type"]; })
            updateFiltersList(sensors);

            for(var index in indexes){
                var nTr = oTable.fnSettings().aoData[ indexes[index] ].nTr;
                $(nTr).addClass('sensor_'+sensors[index]);
            }
            newestAdded=$.map(logs, function(o) { return o["time"]; }).sort().pop();
        }
            setTimeout(queryNewData,100);
        }).fail(function (e) {
            alert(e.responseText());
            setTimeout(queryNewData,500);
        });
}