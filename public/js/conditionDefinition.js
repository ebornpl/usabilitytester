function animateToEditView(condition) {
    updateContentEditDiv(condition);

    $('#conditionsDiv').fadeOut('fast', function () {
        $('#editDiv').fadeIn('fast');
    });
}

function animateToTableView() {
    if(Object.keys(conditions).length>0){
        $('#conditionDetailsForm').show();
        $('#btnRemoveCondition').show();
    }
    updateConditionsTableDiv();

    $('#editDiv').fadeOut('fast', function () {
        $('#conditionsDiv').fadeIn('fast');
    });
}

function updateConditionsTableDiv() {
    var result="";
    for (var conditionIndex in conditions){
        var condition=conditions[conditionIndex];
        result+='<tr><td><input type="checkbox"></td><td>'+condition.Id+'</td><td>'+condition.Name+'</td></tr>';
    }
    $('#conditionTable > tbody:last').html(result);
}

function updateContentEditDiv(condition) {
    $('#conditionID').text(condition.Id);
    $('#conditionName').val(condition.Name);
    $('#conditionDescription').val(condition.Description);
    $('#conditionStartCondition').val(condition.StartCondition);
    $('#conditionEndCondition').val(condition.EndCondition);
}

var conditions={};

$(document).ready(function () {
    $('#conditionTable tbody').on("click","tr", function () {
        animateToEditView(conditions[$("td:eq(1)",this).text()]);
    });

    $('#btnNewCondition').click(function(){
        var newCondition = {
            Id: Math.floor((Math.random()*10000)+1),
            Name: '-',
            Description: '-',
            StartCondition: '-',
            EndCondition: '-'
        };

        animateToEditView(newCondition);
    });

    $('#btnSaveCondition').click(function(){
        var savedCondition = {
            Id: $('#conditionID').text(),
            Name: $('#conditionName').val(),
            Description: $('#conditionDescription').val(),
            StartCondition: $('#conditionStartCondition').val(),
            EndCondition: $('#conditionEndCondition').val()
        };
        conditions[savedCondition.Id]=savedCondition;

        animateToTableView();
    });

    $('#btnCancelCondition').click(function(){
        animateToTableView();
    });
});