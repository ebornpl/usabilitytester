function animateToEditView(condition) {
    updateContentEditDiv(condition);

    $('#conditionsDiv').fadeOut('fast', function () {
        $('#editDiv').fadeIn('fast');
    });
}

function animateToTableView() {
    updateConditionsTableDiv();

    $('#editDiv').fadeOut('fast', function () {
        $('#conditionsDiv').fadeIn('fast');
    });
}

function updateConditionsTableDiv() {
    var result = "";

    $.get("./conditions/list", function (conditions) {
        for (var index in conditions) {
            result += '<tr><td><input type="hidden" value="' + conditions[index].id + '">' + conditions[index].name + '</td></tr>';
        }
    }).fail(function (e) {
        result = "error obtaining items";
    }).always(function () {
        $('#conditionTable > tbody:last').html(result);
    });
}

function updateContentEditDiv(condition) {
    $('#conditionId').val(condition.id);
    $('#conditionName').val(condition.name);
    $('#conditionDescription').val(condition.description);
}

$(document).ready(function () {
    $('#conditionTable tbody').on("click", "tr", function (e) {
        var id = $("input[type='hidden']", this).val();
            $.ajax({
                type: 'GET',
                url: './conditions/' + id,
                dataType: "json"
            }).done(function (condition) {
                animateToEditView(condition);
            }).fail(function () {
                alert("ajax error");
            })
    });

    $('#btnCancelCondition').click(function (e) {
        e.preventDefault();
        animateToTableView();
    });

    $('#btnSaveCondition').click(function (e) {
        e.preventDefault();
        var savedCondition = {
            name: $('#conditionName').val(),
            description: $('#conditionDescription').val()
        };
        var id = $('#conditionId').val();
        var append = '';
        if (id != '') append = '/' + id;

        $.ajax({
            url: './conditions' + append,
            type: 'POST',
            data: savedCondition
        }).done(function (e) {
            animateToTableView();
        }).fail(function (e) {
            alert(e);
        });
    });
});