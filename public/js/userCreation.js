function animateToEditView(participant) {
    updateContentEditDiv(participant);

    $('#participantsDiv').fadeOut('fast', function () {
        $('#editDiv').fadeIn('fast');
    });
}

function animateToTableView() {
    if(Object.keys(participants).length>0){
        $('#participantDetailsForm').show();
        $('#btnRemoveParticipant').show();
    }
    updateParticipantsTableDiv();

    $('#editDiv').fadeOut('fast', function () {
        $('#participantsDiv').fadeIn('fast');
    });
}

function updateParticipantsTableDiv() {
    var result="";
    for (var participantIndex in participants){
        var participant=participants[participantIndex];
        result+='<tr><td><input type="checkbox"></td><td>'+participant.Id+'</td><td>'+participant.Name+'</td></tr>';
    }
    $('#participantTable > tbody:last').html(result);
}

function updateContentEditDiv(participant) {
    $('#participantID').text(participant.Id);
    $('#participantName').val(participant.Name);
    $('#participantDescription').val(participant.Description);
    $('#participantStartCondition').val(participant.StartCondition);
    $('#participantEndCondition').val(participant.EndCondition);
}

var participants={};

$(document).ready(function () {
    $('#participantTable tbody').on("click","tr", function () {
        animateToEditView(participants[$("td:eq(1)",this).text()]);
    });

    $('#btnNewParticipant').click(function(){
        var newParticipant = {
            Id: Math.floor((Math.random()*10000)+1),
            Name: '-',
            Description: '-',
            StartCondition: '-',
            EndCondition: '-'
        };

        animateToEditView(newParticipant);
    });

    $('#btnSaveParticipant').click(function(){
        var savedParticipant = {
            Id: $('#participantID').text(),
            Name: $('#participantName').val(),
            Description: $('#participantDescription').val(),
            StartCondition: $('#participantStartCondition').val(),
            EndCondition: $('#participantEndCondition').val()
        };
        participants[savedParticipant.Id]=savedParticipant;

        animateToTableView();
    });

    $('#btnCancelParticipant').click(function(){
        animateToTableView();
    });
});