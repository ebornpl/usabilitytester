var runningCounter;

function handleActionSubmitted(taskSessionId, actionType) {
    var stateNodeForTaskSession = $('#tasksessionsTable tbody :hidden:nth-child(1)[value=' + taskSessionId + '] + :hidden');
    var oldState = stateNodeForTaskSession.val();
    var oldRunningTr = getRunningTr();
    //update the state
    stateNodeForTaskSession.val(actionType + 1);
    switch (actionType) {
        case 0:
            //run
            startTimerIfNecessary();
            var runningTr = getRunningTr();
            var buttons = runningTr.find('button');
            $(buttons[0]).prop('disabled', true);
            $(buttons[1]).prop('disabled', false);
            $(buttons[2]).prop('disabled', false);
            if (oldState == 0) {
                //no task was running - disable all others
                $('#tasksessionsTable tbody tr').not(runningTr).find('button').prop('disabled', true);
                runningTr.find('a').show();
                var d = new Date();
                var dateTimeText = d.getFullYear() + '-' + pad((d.getMonth() + 1).toString(), 2) + '-' + pad(d.getDate().toString(), 2);
                dateTimeText += " " + pad(d.getHours().toString(), 2) + ":" + pad(d.getMinutes().toString(), 2) + ":" + pad(d.getSeconds().toString(), 2);
                runningTr.find('td:nth-child(3)').text(dateTimeText);
            }
            break;
        case 1:
            //pause
            clearInterval(runningCounter);
            var buttons = oldRunningTr.find('button');
            $(buttons[0]).prop('disabled', false);
            $(buttons[1]).prop('disabled', true);
            $(buttons[2]).prop('disabled', false);
            break;
        case 2:
            //stop
            clearInterval(runningCounter);
            //refresh states of all
            $('#tasksessionsTable tbody tr').each(function () {
                var state = $(this).find(':hidden:nth-child(2)').val();
                var buttons = $(this).find('button');
                //only to possibilities at this point - either stopped or not yet started
                if (state == 0) {
                    $(buttons[0]).prop('disabled', false);
                    $(buttons[1]).prop('disabled', true);
                    $(buttons[2]).prop('disabled', true);
                } else {
                    $(buttons[0]).prop('disabled', true);
                    $(buttons[1]).prop('disabled', true);
                    $(buttons[2]).prop('disabled', true);
                }
            });
            break;
    }

}

function startTimerIfNecessary() {
    var runningTr = getRunningTr();
    if (runningTr.length > 0) {
        var tdToUpdate = runningTr.find('td:nth-child(4)');
        runningCounter = setInterval(function () {
            increaseTimer(tdToUpdate)
        }, 1000);
    }
}

function increaseTimer(td) {
    var timeParts = td.text().split(':');

    if (timeParts[2] == 59) {
        //seconds
        timeParts[2] = "00"
        if (timeParts[1] == 59) {
            //minutes
            timeParts[1] = "00"
            //hours
            timeParts[0]++;
            timeParts[0] = pad(timeParts[0].toString(), 2);
        }
        else {
            timeParts[1]++;
            timeParts[1] = pad(timeParts[1].toString(), 2);
        }
    }
    else {
        timeParts[2]++;
        timeParts[2] = pad(timeParts[2].toString(), 2);
    }
    td.text(timeParts.join(':'));
}

function pad(str, max) {
    return str.length < max ? pad("0" + str, max) : str;
}

function getRunningTr() {
    return $('#tasksessionsTable tbody :hidden:nth-child(2)[value=1]').closest('tr');
}

$(document).ready(function () {
    startTimerIfNecessary();

    $('.btn-small').click(function () {
        var id = $(this).closest("tr").find(":hidden:first-child").val();
        var actionIndex = $(this).index();

        $.ajax({
            url: './handleClick',
            type: 'POST',
            data: {
                taskSessionId: id,
                actionType: actionIndex
            }
        }).done(function () {
                handleActionSubmitted(id, actionIndex);
            }).fail(function (e) {
                alert(e.responseText);
            })
    });
});