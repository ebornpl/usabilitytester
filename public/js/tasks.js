function animateToEditView(task) {
    updateContentEditDiv(task);

    $('#tasksDiv').fadeOut('fast', function () {
        $('#editDiv').fadeIn('fast');
    });
}

function animateToTableView() {
    updateTasksTableDiv();

    $('#editDiv').fadeOut('fast', function () {
        $('#tasksDiv').fadeIn('fast');
    });
}

function updateTasksTableDiv() {
    var result = "";

    $.get("./tasks/list", function (tasks) {
        for (var index in tasks) {
            result += '<tr><td><input type="checkbox" class="checkbox text-center"></td><td><input type="hidden" value="' + tasks[index].id + '">' + tasks[index].name + '</td></tr>';
        }
    }).fail(function (e) {
        result = "error obtaining items";
    }).always(function () {
        $('#taskTable > tbody:last').html(result);
        showTableElementsIfNeeded();
    });
}

function updateContentEditDiv(task) {
    $('#taskId').val(task.id);
    $('#taskName').val(task.name);
    $('#taskDescription').val(task.description);
    $('#taskStartCondition').val(task.start_condition);
    $('#taskEndCondition').val(task.end_condition);
}

function showTableElementsIfNeeded() {
    if ($('#taskTable > tbody:last').has('tr').length > 0) {
        $("#tasksDiv h4").html("Detected tasks");
        $('#taskDetailsForm').show();
        $('#btnRemoveTask').show();

    } else {
        $("#tasksDiv h4").html("You have no tasks! Please add some here")
        $('#taskDetailsForm').hide();
        $('#btnRemoveTask').hide();
    }
}

$(document).ready(function () {
    showTableElementsIfNeeded();

    $('#taskTable tbody').on("click", "tr", function (e) {
        if (e.target.type == "checkbox") {
            // stop the bubbling to prevent firing the row's click event
            e.stopPropagation();
        } else {
            var id = $("input[type='hidden']", this).val();
            $.ajax({
                type: 'GET',
                url: './tasks/' + id,
                dataType: "json"
            }).done(function (task) {
                animateToEditView(task);
            }).fail(function () {
                alert("ajax error");
            })
        }
    });

    $('#btnNewTask').click(function () {
        var newTask = {
            id: '',
            name: '',
            description: '',
            start_condition: '',
            end_condition: ''
        };

        animateToEditView(newTask);
    });

    $('#btnSaveTask').click(function (e) {
        e.preventDefault();
        var savedTask = {
            name: $('#taskName').val(),
            description: $('#taskDescription').val(),
            start_condition: $('#taskStartCondition').val(),
            end_condition: $('#taskEndCondition').val()
        };
        var id = $('#taskId').val();
        var append = '';
        if (id != '') append = '/' + id;
        $.ajax({
            url: './tasks' + append,
            type: 'POST',
            data: savedTask
        }).done(function () {
            animateToTableView();
        }).fail(function (e) {
            alert(e);
        })
    });

    $('#btnCancelTask').click(function (e) {
        e.preventDefault();
        animateToTableView();
    });

    $('#btnRemoveTask').click(function (e) {
        var selectedTasks = $('#taskDetailsForm tr').has(':checkbox:checked').find('input[type="hidden"]').map(function () {
            return $(this).val();
        }).get();

        $.ajax({
            url: './tasks/delete',
            type: 'POST',
            data: {tasks: selectedTasks}
        }).done(function (e) {
            updateTasksTableDiv();
        }).fail(function (e) {
            alert(e);
        })
    });
});