/**
 * Created with IntelliJ IDEA.
 * User: eborn
 * Date: 27.05.2013
 * Time: 11:00
 * To change this template use File | Settings | File Templates.
 */
function getURLParameter(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
}

function fillSessionList(){
    var content="";
    for(var i=0;i<5;i++)
        content+="<li><a href='testExecution.php?"+urlParams+"&sessionIndex="+i+"'>Session "+i+" - "+users[i]+"</a></li>"
    $("#sessionsList").html(content)
}

function unique(array){
    return $.grep(array,function(el,index){
        return index == $.inArray(el,array);
    });
}

var urlParams="";
var users= ["Bartek", "Paulina", "Fat guy","Cute girl", "Old lady"];

$(document).ready(function () {
    $('input[name="conditions"]').change(function(){
        if ($(this).index('input[name="conditions"]')==1){
            $("#subjectNumber").prop("disabled",false);
            $("#betweenSubjectParagraph").show();
        }  else {
            $("#subjectNumber").prop("disabled",true);
            $("#betweenSubjectParagraph").hide();
        }
    });

    $('#saveProjectButton').click(function () {
        var products, between_subject;
        if($('#settingsModal input[name=conditions]:checked').val()=='multi' && $('#subjectNumber').val()>1){
            products=$('#subjectNumber').val();
            between_subject=$('#settingsModal input[name=between]').is(':checked')?1:0;
        }
        else{
            products=1;
            between_subject=0;
        }


        var savedProject = {
            name: $('#projectTitle').val(),
            author: $('#projectAuthor').val(),
            description: $('#projectDescription').val(),
            products: products,
            between_subject: between_subject
        };
        var id = $('#projectId').val();
        var append = '';
        if (id != '') append = '/' + id;
        $.ajax({
            url: './projects' + append,
            type: 'POST',
            data: savedProject
        }).done(function () {
                window.location = window.location.pathname;
            }).fail(function (e) {
                alert(e.responseText);
            })
    });

    $("#sessionsList").hide();
    fillSessionList();

    $("#menuSidebar").click(function(e){
        if(/^\t*Test execution/.test($(this).text())){
        e.preventDefault();
        $("#sessionsList").toggle();
        }
    })
});