<?php

class TaskController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@index');
	|
	*/

	public function index()
	{
		$project = Project::with('tasks')->find(Session::get('project_id'));
        $menuItems=Helpers::generateMenuLinksArray($project,Helpers::pageTask);
		return View::make('task')->with('project',$project)->with('menuItems', $menuItems);
	}

	public function getTask($task_id)
	{
		return Response::json(Task::find($task_id));
	}

	public function getAll()
	{
		return Response::json(Task::where('project_id','=', Session::get('project_id'))->orderBy('name')->get());
	}

	public function deleteTasks()
	{
		$tasksToDelete=Input::get('tasks');
		Task::whereIn('id',$tasksToDelete)->delete();
	}

	public function postNewTask()
	{
		$taskReceived=Input::all();
        $taskReceived['project_id']=Session::get('project_id');
		$task = Task::create($taskReceived);
		$task->save();
        $this::assignNewTaskToAllSessions($task, Session::get('project_id'));
	}

	public function postExisitingTask($task_id)
	{
		$task=Task::find($task_id);
		foreach (Input::all() as $key => $value) {
			$task[$key]=$value;
		}
		$task->save();
	}

    public static function assignNewTaskToAllSessions($task, $project_id){
        $project = Project::with('conditions.usersessions')->find($project_id);
        $userSessionIds=array();
        foreach($project->conditions as $condition)
            $userSessionIds = array_merge($userSessionIds, $condition->usersessions->lists('id'));
        $task->usersessions()->sync($userSessionIds);
    }

}