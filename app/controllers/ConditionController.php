<?php

class ConditionController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@index');
	|
	*/

	public function index()
	{
		$project = Project::with('conditions')->find(Session::get('project_id'));
        $menuItems=Helpers::generateMenuLinksArray($project,Helpers::pageCondition);
		return View::make('condition')->with('menuItems', $menuItems)->with('project',$project);
	}

	public function getAll()
	{
		return Response::json(Condition::where('project_id','=', Session::get('project_id'))->get());
	}
	
	public function postExistingCondition($condition_id) {
		$condition=Condition::find($condition_id);
		foreach (Input::all() as $key => $value) {
			$condition[$key]=$value;
		}
		$condition->save();
	}

	public function getCondition($condition_id)
	{
		return Response::json(Condition::find($condition_id));
	}

    public static function assignAllUsersToCondition($condition, $project_id){
        $project = Project::with('tasks', 'conditions')->find($project_id);
        $condition->users()->sync($project->users->lists('id'));
        //Also assign all the tasks to the newly created sessions
        foreach($condition->usersessions as $session){
            $session->tasks()->sync($project->tasks->lists('id'));
        }
    }

}