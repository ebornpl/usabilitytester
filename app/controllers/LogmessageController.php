<?php

class LogmessageController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@index');
	|
	*/

	public function index()
	{
        $project = Project::with('users.conditions', 'conditions')->find(Session::get('project_id'));
        $menuItems=Helpers::generateMenuLinksArray($project,Helpers::pageExecute);
		return View::make('sessionlist')->with('menuItems', $menuItems)->with('project',$project);
	}

    public function viewLog($taskSession_id){
        $taskSession= Tasksession::with('logmessages')->find($taskSession_id);
        $project = Project::find(Session::get('project_id'));
        $menuItems=Helpers::generateMenuLinksArray($project,Helpers::pageExecute);
        return View::make('log')->with('menuItems', $menuItems)->with('taskSession', $taskSession)->with('project', $project);
    }

	public function postLogs(){
		$logs=json_decode(Input::get('data'),true);
		//return var_dump($logs);
		foreach ($logs as $logMessage) {
            $currentlyOpenedTask=Tasksession::where('state', '=', '1')->first();
			$log=Logmessage::create($logMessage);
			$currentlyOpenedTask->logmessages()->save($log);
		}
	}

    public function postComment(){
        Logmessage::create(Input::all())->save();
    }

    public function getNewItems(){
        $taskSession=Tasksession::find(Input::get('tasksession_id'));
        $time=Input::get('time');
        $newLogItems=$taskSession->logmessages()->where('time', '>', $time)->get();
        return Response::json($newLogItems);
    }
}