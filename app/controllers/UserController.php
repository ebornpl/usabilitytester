<?php

class UserController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@index');
	|
	*/

	public function index()
	{
		$project = Project::with('users')->find(Session::get('project_id'));
        $menuItems=Helpers::generateMenuLinksArray($project,Helpers::pageUser);
		return View::make('user')->with('menuItems', $menuItems)->with('project',$project);
	}

	public function getUser($user_id)
	{
		return Response::json(User::find($user_id));
	}

	public function getAll()
	{
		return Response::json(User::where('project_id','=', Session::get('project_id'))->get());
	}

	public function deleteUsers()
	{
		$usersToDelete=Input::get('users');
		User::whereIn('id',$usersToDelete)->delete();
	}

	public function postNewUser()
	{
		$userReceived=Input::all();
		$userReceived['project_id']=Session::get('project_id');
		$user = User::create($userReceived);
		$user->save();
        $project = Project::with('users')->find(Session::get('project_id'));
        //sync all the conditions with new users
        if($project['between_subject']==0)
            SessionController::assignAllConditionsToUser($user, Session::get('project_id'));
	}

	public function postExisitingUser($user_id)
	{
		$user=User::find($user_id);
		foreach (Input::all() as $key => $value) {
			$user[$key]=$value;
		}
		$user->save();
	}

}