<?php

class ProjectController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@index');
	|
	*/

	public function index()
	{
        $pages['project']=1;
		$projects=Project::all();
        $project=null;
		return View::make('home')->with('projects', $projects)->with('project',$project);
	}


    public function projectSelected($project_id)
    {
		Session::put('project_id', $project_id);
        $project=Project::find($project_id);
        if($project['products']>1)
            return Redirect::action('ConditionController@index');
        else
		    return Redirect::action('TaskController@index');
	}

    public function postNewProject()
    {
        $projectReceived=Input::all();
        $project = Project::create($projectReceived);
        $project->save();
        for($i=0;$i<$projectReceived['products'];$i++) {
            $condition = Condition::create(array('name'=>'Cond'.$i));
            $project->conditions()->save($condition);
        }

    }

    public function postExisitingProject($project_id)
    {
        $project=Project::find($project_id);
        foreach (Input::all() as $key => $value) {
            if($key=='products' && $project[$key]!=$value){
                //Remove all the conditions assigned:
                $project->conditions()->delete();
                //add new ones
                for($i=0;$i<$value;$i++) {
                    $condition = Condition::create(array('name'=>'Condition '.($i+1)));
                    $project->conditions()->save($condition);
                    ConditionController::assignAllUsersToCondition($condition,$project_id);
                }
                //

            }
            if($key=='between_subject' && $project[$key]!=$value)
            {
                foreach($project->users as $user){
                    if($value!=1)
                    //sync all the projects with all users
                        SessionController::assignAllConditionsToUser($user,$project_id);
                    else
                        //else unlink all
                        $user->conditions()->sync(array());
                }
            }
            $project[$key]=$value;
        }
        $project->save();
    }
}