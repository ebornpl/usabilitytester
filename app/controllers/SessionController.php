<?php
/**
 * Created by IntelliJ IDEA.
 * User: eborn
 * Date: 05.07.2013
 * Time: 00:18
 * To change this template use File | Settings | File Templates.
 */

class SessionController extends BaseController
{

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@index');
    |
    */

    public function index()
    {
        $project = Project::with('users.conditions', 'conditions')->find(Session::get('project_id'));
        $menuItems = Helpers::generateMenuLinksArray($project, Helpers::pageAssign);
        return View::make('assignment')->with('project', $project)->with('menuItems', $menuItems);
    }

    public function updateSessions()
    {
        foreach (Input::all() as $userInput => $conditionId) {
            $userId = substr($userInput, 4);
            $user = User::find($userId);
            $user->conditions()->sync(array($conditionId));
            $project = Project::with('tasks')->find(Session::get('project_id'));
            foreach ($user->usersessions as $session) {
                $session->tasks()->sync($project->tasks->lists('id'));
            }
        }
        return $this->index();
    }

    public static function assignAllConditionsToUser($user, $project_id)
    {
        $project = Project::with('tasks', 'conditions')->find($project_id);
        $user->conditions()->sync($project->conditions->lists('id'));
        //Also assign all the tasks to the newly created sessions
        foreach ($user->usersessions as $session) {
            $session->tasks()->sync($project->tasks->lists('id'));
        }
    }

    public function taskSessionsIndex($userSession_id)
    {
        $usersession = Usersession::with('tasksessions')->find($userSession_id);
        $project = Project::find(Session::get('project_id'));
        $menuItems = Helpers::generateMenuLinksArray($project, Helpers::pageExecute);
        $indexOfRunningOrPausedTask = -1;
        foreach ($usersession->tasksessions as $tasksession) {
            if (in_array($tasksession['state'], array(1, 2))) {
                $indexOfRunningOrPausedTask = $tasksession['id'];
                if($tasksession['state']==1){
                    //if it's running, it means there was a refresh -> update duration
                    $lastUpdated = strtotime($tasksession['updated_at']);
                    $tasksession['duration']=$tasksession['duration']+(time()-$lastUpdated);
                    $tasksession->save();
                }
                break;
            }
        }
        //We need to also check if some other sessions have open tasks:
        $externalTaskRunning=false;
        if($indexOfRunningOrPausedTask==-1){
            $externalTaskSessionRunning=Tasksession::whereIn('state', array(1,2))->first();
            if(!empty($externalTaskSessionRunning))
                $externalTaskRunning=true;
        }
        return View::make('session', array('menuItems' => $menuItems, 'usersession' => $usersession, 'project' => $project, 'indexOfRunningOrPausedTask' => $indexOfRunningOrPausedTask, 'externalTaskRunning'=>$externalTaskRunning));
    }

    public function handleTaskSessionClick()
    {
        $actionType = Input::get('actionType');
        $taskSession = Tasksession::find(Input::get('taskSessionId'));

        if (empty($taskSession))
            App::abort(500, "Wrong tasksessionId!");
        switch ($actionType) {
            //play button
            case 0:
                if (in_array($taskSession['state'], array(1, 3)))
                    App::abort(500, "You can't start stopped or running task");
                $taskSession['state']=1;
                if(empty($taskSession['starttime']))
                    $taskSession['starttime']=date('Y-m-d H:i:s');
                $taskSession->save();
                break;
            //pause button
            case 1:
                if (!$taskSession['state']==1)
                    App::abort(500, "You can only pause running task");
                $taskSession['state']=2;
                $lastUpdated = strtotime($taskSession['updated_at']);
                $taskSession['duration']=$taskSession['duration']+(time()-$lastUpdated);
                $taskSession->save();
                break;
            //stop button
            case 2:
                if (in_array($taskSession['state'], array(0, 3)))
                    App::abort(500, "You can only stop paused or running task");
                if($taskSession['state']==1){
                    //if it was running we have to update the duration first
                    $lastUpdated = strtotime($taskSession['updated_at']);
                    $taskSession['duration']=$taskSession['duration']+(time()-$lastUpdated);
                }
                $taskSession['state']=3;
                $taskSession->save();
                break;
        }
    }
}
