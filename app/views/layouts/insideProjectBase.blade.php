@extends('layouts.base')

@section('navBarContent')
    <a class="brand" href="#">{{ $project['name'] }}</a>
        <ul class="nav">
            <li><a href="{{ action('ProjectController@index'); }}"><i class="icon-home icon-white"></i> Home</a></li>
            <li><a href="#" data-toggle="modal" data-target="#settingsModal"><i class="icon-cog icon-white"></i> Settings</a></li>
        </ul>
@stop

@section('content')
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span3 offset1">
            <div class="well sidebar-nav" id="menuSidebar">
                <ul class="nav nav-list">
                    @foreach($menuItems as $menuItem)
                    <li @if($menuItem['selected']) class='active' @endif >{{ link_to_action($menuItem['action'], $menuItem['name']); }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="span7 hero-unit" id="heroContent">
		@yield('heroUnit')        
        </div>
    </div>
</div>
@stop

