<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
<title>From Design to Software</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
{{ HTML::style('http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css') }}
{{ HTML::style('css/general.css') }}

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<![endif]-->
@yield('customCss') 
</head>

<body>

<div class="navbar navbar-inverse">
    <div class="navbar-inner">
        @section('navBarContent')   
        <a class="brand" href="#">Usability testing tool</a>
        @show
    </div>
</div>

@yield('content')

<div id="settingsModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Project properties</h3>
    </div>
    <div class="modal-body">
        <form class="form-horizontal" action="" method="get">
            <input type="hidden" name="id" id="projectId" value="{{ $project['id'] }}">
            <div class="control-group">
                <label class="control-label" for="projectTitle">Title</label>
                <div class="controls">
                    <input type="text" id="projectTitle" name="projectTitle" placeholder="Title" value="{{ $project["name"] }}">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="projectAuthor">Author</label>
                <div class="controls">
                    <input type="text" id="projectAuthor" name="projectAuthor" placeholder="Author" value="{{ $project["author"] }}">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="projectDescription">Description</label>
                <div class="controls">
                    <input type="text" id="projectDescription" name="projectDescription" placeholder="Description" value="{{ $project["description"] }}">
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <label class="radio">
                        <input type="radio" name="conditions" @if($project['products']==1 || $project==null) checked="true" @endif value="single"/> I will be testing a single product.
                    </label>
                    <label class="radio">
                        <input type="radio" name="conditions" @if($project['products']>1) checked="true" @endif value="multi"/>
                        I want to compare  <input class="input-mini" type="text" id="subjectNumber" name="subjectNumber" @if($project['products']==1 || $project==null) disabled="true" @endif  value="{{ $project['products']}}" /> products.
                    </label>
                    <label class="checkbox" id="betweenSubjectParagraph">
                        <input type="checkbox" name="between" @if($project['between_subject']==1) checked="true" @endif /> Enable between subject testing.
                    </label>
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" id="saveProjectButton">Save changes</button>
    </div>
</div>
<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
{{ HTML::script('http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js') }}
{{ HTML::script('http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js') }}
<!--Jquery Scripts come here-->
{{ HTML::script('js/general.js') }}
@yield('customJs') 

</body>
</html>
