@extends('layouts.insideProjectBase')

@section('customCss')
{{ HTML::style('css/log.css') }}
{{ HTML::style('css/DT_bootstrap.css') }}
@stop

@section('customJs')
{{ HTML::script('js/log.js') }}
{{ HTML::script('http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js') }}
{{ HTML::script('js/DT_bootstrap.js') }}
@stop

@section('heroUnit')
<input type="hidden" value="{{ $taskSession['id']; }}" id='taskSessionId'>
<div id="executionsDiv">
    <br />
    @if(count($project->users)==0)
    <h4>You have no users! Please add some first</h4>
    @else
    <table class="table table-bordered" id="logmessagesTable">
        <thead>
        <tr>
            <th class="span10">Message</th>
            <th class="span1">Sensor</th>
            <th class="span1">Time</th>
        </tr>
        </thead>
        <tbody>
        @foreach($taskSession->logmessages as $logmessage)
        <tr class="sensor_{{ $logmessage['sensor_type']; }}"><td>{{ $logmessage['message'] }}</td><td>{{ $logmessage['sensor_type'] }}</td><td>{{ $logmessage['time'] }}</td></tr>
        @endforeach
        </tbody>
    </table>
    <div class="form-inline">
    <input class="span10" type="text" placeholder="Add a comment..." id="commentTextBox">
        <input type="button" class="btn btn-primary span2" value="Submit" id="commentButton">
    </div>
    <h4>Filters:</h4>
    <div id="filters">
        <input type="button" id="filtersToggleButton" class="btn btn-mini" value="Select all/none">
        </div>
    @endif
</div>
@stop