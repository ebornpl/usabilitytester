@extends('layouts.insideProjectBase')

@section('customCss')
{{ HTML::style('css/tasks.css') }}
@stop

@section('customJs')
{{ HTML::script('js/execution.js') }}
@stop

@section('heroUnit')
<div id="executionsDiv" class="text-center">
    <br />
    <h4>{{session->condition["name"]}} - {{session->user["name"]}}</h4>
    <form id="executionDetailsForm">
        <table class="table table-striped" id="logsTable">
            <tbody>
                <tr>
                    <th class="span3">Task name</th>
                    <th class="span2"></th>
                    <th class="span3">Start time</th>
                    <th class="span3">End time</th>
                    <th class="span1">Log</th>
                </tr>
                @foreach ($session->tasks as $task)
                    <tr><td>{{$task["name"]}}</td><td>Buttony</td><td></td><td></td><td>LogLink</td></tr>
                @endforeach
            </tbody>
        </table>
    </form>
</div>
@stop