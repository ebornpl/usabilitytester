@extends('layouts.insideProjectBase')

@section('customCss')
{{ HTML::style('css/sessionlist.css') }}
@stop

@section('customJs')
{{ HTML::script('js/execution.js') }}
@stop

@section('heroUnit')
<div id="executionsDiv">
    <br />
    @if(count($project->users)==0)
    <h4>You have no users! Please add some first</h4>
    @else
    <h4>Sessions</h4>
        <div class="accordion" id="accordion2">
            @foreach($project->conditions as $condition)
            <div class="accordion-group">
                <div class="accordion-heading">
                  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#Condition{{ $condition['id'] }}">
                    {{ $condition['name'] }}
                  </a>
                </div>
                <div id="Condition{{ $condition['id'] }}" class="accordion-body collapse">
                  <div class="accordion-inner">
                      <ul class="nav nav-tabs nav-stacked">
                          @foreach($condition->users as $user)
                          <li>{{ link_to_action('SessionController@taskSessionsIndex', $user['name'], $user->pivot->id) }}</li>
                          @endforeach
                      </ul>
                  </div>
                </div>
            </div>
            @endforeach
        </div>
    @endif
</div>
@stop