@extends('layouts.insideProjectBase')

@section('customCss')
{{ HTML::style('css/tasks.css') }}
@stop

@section('customJs')
{{ HTML::script('js/tasks.js') }}
@stop

@section('heroUnit')
<div id="tasksDiv" class="text-center">
    @if(count($project->conditions)==0)
    <h4>You have no tasks! Please add some here</h4>
    @else
    <h4>Defined tasks</h4>
    @endif
    <br />
    <form id="taskDetailsForm">
        <table class="table table-striped table-bordered table-hover" id="taskTable">
            <thead>
            <tr>
                <th class="span1"></th>
                <th class="span11">Name</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($project->tasks as $task)
            <tr><td><input type="checkbox" class="checkbox text-center"></td><td><input type="hidden" value="{{$task["id"]}}"> {{$task["name"]}}</td></tr>
            @endforeach
            </tbody>
        </table>
    </form>
    <button class="btn" id="btnRemoveTask">Remove selected</button>
    <button class="btn" id="btnNewTask">New task</button>
</div>
<div id="editDiv">
    <form class="form-horizontal offset1 span10" action="" method="get">
        <input type="hidden" name="taskId" id="taskId" value="">
        <div class="control-group">
            <label class="control-label" for="taskName">Name</label>
            <div class="controls">
                <input type="text" id="taskName" name="taskName" placeholder="Task name">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="taskDescription">Description</label>
            <div class="controls">
                <textarea id="taskDescription" name="taskDescription" rows='3' placeholder="Description of the task"></textarea>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="taskStartCondition">Start condition</label>
            <div class="controls">
                <textarea id="taskStartCondition" name="taskStartCondition" rows='3' placeholder="e.g. Several items in the basket"></textarea>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="taskEndCondition">End condition</label>
            <div class="controls">
                <textarea id="taskEndCondition" name="taskEndCondition" rows='3' placeholder="e.g. Finished purchase"></textarea>
            </div>
        </div>
        <div class="controls">
            <button class="btn" id="btnCancelTask">Cancel</button>
            <button class="btn btn-primary" id="btnSaveTask">Save changes</button>
        </div>
    </form>
</div>
@stop