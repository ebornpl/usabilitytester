@extends('layouts.insideProjectBase')

@section('customCss')
{{ HTML::style('css/tasks.css') }}
@stop

@section('customJs')
{{ HTML::script('js/users.js') }}
@stop

@section('heroUnit')
<div id="usersDiv" class="text-center">
    @if(count($project->users)==0)
    <h4>You have no users! Please add some here</h4>
    @else 
    <h4>Defined users</h4>
    @endif
    <br />
    <form id="userDetailsForm">
        <table class="table table-striped table-bordered table-hover" id="userTable">
            <thead>
            <tr>
                <th class="span1"></th>
                <th class="span3">Name</th>
                <th class="span1">Age</th>
                <th class="span7">Occupation</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($project->users as $user)
            <tr><td><input type="checkbox" class="checkbox text-center"></td><td><input type="hidden" value="{{$user["id"]}}"> {{$user["name"]}}</td><td>{{$user["age"]}}</td><td>{{$user["occupation"]}}</td></tr>
            @endforeach
            </tbody>
        </table>
    </form>
    <button class="btn" id="btnRemoveUser">Remove selected</button>
    <button class="btn" id="btnNewUser">New participant</button>
</div>
<div id="editDiv">
    <form class="form-horizontal offset1 span10" action="" method="get">
        <input type="hidden" name="userId" id="userId" value="">
        <div class="control-group">
            <label class="control-label" for="userName">Name</label>
            <div class="controls">
                <input type="text" id="userName" name="userName" placeholder="User name">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="userAge">Age</label>
            <div class="controls">
                <textarea id="userAge" name="userAge" rows='1' placeholder="Age of the user"></textarea>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="userOccupation">Occupation</label>
            <div class="controls">
                <textarea id="userOccupation" name="userOccupation" rows='1' placeholder="Occupation of the user"></textarea>
            </div>
        </div>
        <div class="controls">
            <button class="btn" id="btnCancelUser">Cancel</button>
            <button class="btn btn-primary" id="btnSaveUser">Save changes</button>
        </div>
    </form>
</div>
@stop