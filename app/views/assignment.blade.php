@extends('layouts.insideProjectBase')

@section('heroUnit')
    <div id="tasksDiv" class="text-center">
@if(count($project->users)==0)
        <h4>Add some users first!</h4>
@else
        <h4>Assignments:</h4>
@endif
        <form id="assignmentsForm" action="{{ URL::action('SessionController@updateSessions')}}" method="post">
            <table class="table table-striped table-bordered table-hover" id="taskTable">
                <thead>
                <tr>
                    <th class="span3">Participant</th>
                    @foreach ($project->conditions as $condition)
                    <th>{{ $condition['name'] }}</th>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                @foreach ($project->users as $user)
                <tr><td>{{ $user['name'] }}</td>
                @foreach ($project->conditions as $condition)
                <td class="text-center"><input type="radio" name="user{{ $user['id'] }}" class="offset5 span12" value="{{$condition['id']}}" @if(array($condition['id'])==$user->conditions()->lists('condition_id')) checked="true" @endif ></td>
                @endforeach
                </tr>
                @endforeach
                </tbody>
            </table>
            <input class="btn" type="submit" value="Save">
        </form>
    </div>
@stop