@extends('layouts.base')

@section('customCss')
{{ HTML::style('css/home.css') }}
@stop

@section('customJs')
{{ HTML::style('css/home.css') }}
@stop

@section('content')
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span3 offset2">
            <div class="well sidebar-nav">
                <ul class="nav nav-list">
                    <li class="active">
                        <button type="button" class="btn" data-toggle="modal" data-target="#settingsModal">New Project</button>
                    </li>
                    <li class="nav-header">Previous Projects</li>
                    @foreach ($projects as $proj)
    				<li>{{ link_to_action('ProjectController@projectSelected', $proj["name"], array('project_id'=>$proj['id'])); }}</li>
					@endforeach
                </ul>
            </div>
        </div>

        <div class="span5 hero-unit" id="projectDetails">
            <div class="row-fluid" id="welcomeMessage">
                Welcome to the usability testing tool. Please choose one of the projects on the left or create a new one.
            </div>
        </div>
    </div>
</div>
@stop