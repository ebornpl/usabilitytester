@extends('layouts.insideProjectBase')

@section('customCss')
{{ HTML::style('css/tasks.css') }}
@stop

@section('customJs')
{{ HTML::script('js/conditions.js') }}
@stop

@section('heroUnit')
<div id="conditionsDiv" class="text-center">
    <h4>Defined conditions</h4>
    <form id="conditionDetailsForm">
        <table class="table table-striped table-bordered table-hover" id="conditionTable">
            <thead>
            <tr>
                <th class="span11">Name</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($project->conditions as $condition)
            <tr><td><input type="hidden" value="{{$condition["id"]}}"> {{$condition["name"]}}</td></tr>
            @endforeach
            </tbody>
        </table>
    </form>
</div>
<div id="editDiv">
    <form class="form-horizontal offset1 span10" action="" method="get">
        <input type="hidden" name="conditionId" id="conditionId" value="">
        <div class="control-group">
            <label class="control-label" for="conditionName">Name</label>
            <div class="controls">
                <input type="text" id="conditionName" name="conditionName" placeholder="Condition name">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="conditionDescription">Description</label>
            <div class="controls">
                <textarea id="conditionDescription" name="conditionDescription" rows='3' placeholder="Description of the condition"></textarea>
            </div>
        </div>
        <div class="controls">
            <button class="btn" id="btnCancelCondition">Cancel</button>
            <button class="btn btn-primary" id="btnSaveCondition">Save changes</button>
        </div>
    </form>
</div>
@stop