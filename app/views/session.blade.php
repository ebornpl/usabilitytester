@extends('layouts.insideProjectBase')

@section('customCss')
{{ HTML::style('css/tasks.css') }}
@stop

@section('customJs')
{{ HTML::script('js/tasksession.js') }}
@stop

@section('heroUnit')
<div id="executionsDiv" class="text-center">
    <br />
    <h4>{{$usersession->condition["name"]}} - {{$usersession->user["name"]}}</h4>
    <table class="table table-striped" id="tasksessionsTable">
        <thead>
            <th class="span3">Task name</th>
            <th class="span3"></th>
            <th class="span2">Start time</th>
            <th class="span2">Duration</th>
            <th class="span2">Log</th>
        </thead>
        <tbody>
            @foreach ($usersession->tasksessions as $tasksession)
                <tr>
                    <td><input type="hidden" value="{{$tasksession['id']}}"><input type="hidden" value="{{$tasksession['state']}}">{{$tasksession->task["name"]}}</td>
                    <td><button @if($externalTaskRunning || !in_array($indexOfRunningOrPausedTask, array(-1,$tasksession['id'])) || in_array($tasksession['state'], array(1,3)))
                        disabled="disabled"
                        @endif class="btn btn-small"><i class="icon-play"></i></button>
                        <button @if($externalTaskRunning || !in_array($indexOfRunningOrPausedTask, array(-1,$tasksession['id'])) || in_array($tasksession['state'], array(0,2,3)))
                        disabled="disabled"
                        @endif class="btn btn-small"><i class="icon-pause"></i></button>
                        <button @if($externalTaskRunning || !in_array($indexOfRunningOrPausedTask, array(-1,$tasksession['id'])) || in_array($tasksession['state'], array(0,3)))
                        disabled="disabled"
                        @endif  class="btn btn-small"><i class="icon-stop"></i></button>
                    </td>
                    <td>{{$tasksession['starttime']}}</td>
                    <td>{{ Helpers::formatDuration($tasksession['duration']) }}</td>
                    <td><a href="{{ action('LogmessageController@viewLog', $tasksession['id']) }}" @if($tasksession['state']==0) style="display: none;" @endif>Log</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@stop