<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//Home
Route::get('/', "ProjectController@index");
Route::post('/projects', "ProjectController@postNewProject");
Route::post('/projects/{project_id}', "ProjectController@postExisitingProject");
Route::get('/projects/project_selected/{project_id?}', 'ProjectController@projectSelected');

//Tasks
Route::get('/tasks', array('before'=>'projectSelected','uses'=>'TaskController@index'));
Route::get('/tasks/list', array('before'=>'projectSelected','uses'=>'TaskController@getAll'));
Route::get('/tasks/{task_id}', array('before'=>'projectSelected','uses'=>'TaskController@getTask'));
Route::post('/tasks', array('before'=>'projectSelected','uses'=>'TaskController@postNewTask'));
Route::post('/tasks/delete', array('before'=>'projectSelected','uses'=>'TaskController@deleteTasks'));
Route::post('/tasks/{task_id}', array('before'=>'projectSelected','uses'=>'TaskController@postExisitingTask'));

//Test assignment
Route::get('/assign', array('before'=>'projectSelected','uses'=>'SessionController@index'));
Route::post('/assign', array('before'=>'projectSelected','uses'=>'SessionController@updateSessions'));

//Logs
Route::post('/logmessages/comment', 'LogmessageController@postComment');
Route::post('/logmessages', 'LogmessageController@postLogs');
Route::get('/sessions', array('before'=>'projectSelected','uses'=>'LogmessageController@index'));
Route::get("/log/recent", array('before' => 'projectSelected', 'uses' => "LogmessageController@getNewItems"));
Route::get("/log/{taskSession_id}", array('before' => 'projectSelected', 'uses' => "LogmessageController@viewLog"));

//Users
Route::get("/users", array('before' => 'projectSelected', 'uses' => "UserController@index"));
Route::get("/users/list", array('before' => 'projectSelected', 'uses' => "UserController@getAll"));
Route::get("/users/{user_id}", array('before' => 'projectSelected', 'uses' => "UserController@getUser"));
Route::post("/users", array('before' => 'projectSelected', 'uses' => "UserController@postNewUser"));
Route::post("/users/delete", array('before' => 'projectSelected', 'uses' => "UserController@deleteUsers"));
Route::post("/users/{user_id}", array('before' => 'projectSelected', 'uses' => "UserController@postExisitingUser"));

//Conditions
Route::get("/conditions", array('before' => 'projectSelected', 'uses' => "ConditionController@index"));
Route::get("/conditions/list", array('before' => 'projectSelected', 'uses' => "ConditionController@getAll"));
Route::get("/conditions/{condition_id}", array('before' => 'projectSelected', 'uses' => "ConditionController@getCondition" ));
Route::post("/conditions/{condition_id}", array('before' => 'projectSelected', 'uses' => "ConditionController@postExistingCondition"));

//TaskSessions
Route::get("/sessions/tasksessions/list/{session_id}", array('before' => 'projectSelected', 'uses' => "SessionController@getAllTasksessionsForSession"));
Route::get("/sessions/{tasksession_id}", array('before' => 'projectSelected', 'uses' => "SessionController@taskSessionsIndex"));
//Route::get("/sessions/{session_id}", array('before' => 'projectSelected', 'uses' => "SessionController@getSession"));
Route::post("/sessions/handleClick", array('before'=>'projectSelected','uses'=>'SessionController@handleTaskSessionClick'));
