<?php

use Illuminate\Database\Migrations\Migration;

class CreateTasksessionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('tasksessions', function($table)
        {
            $table->increments('id');
            $table->string('task_id')->nullable();
            $table->foreign('task_id')->references('id')->on('tasks')->onDelete('cascade');;
            $table->integer('session_id');
            $table->foreign('session_id')->references('id')->on('usersessions')->onDelete('cascade');;
            $table->integer('state')->default(0);
            $table->dateTime('starttime');
            $table->integer('duration')->default(0);
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('tasksessions');
	}

}