<?php

use Illuminate\Database\Migrations\Migration;

class CreateSessionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('usersessions', function($table)
        {
            $table->increments('id');
            $table->string('condition_id')->nullable();
            $table->foreign('condition_id')->references('id')->on('conditions')->onDelete('cascade');;
            $table->integer('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');;
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('usersessions');
	}

}