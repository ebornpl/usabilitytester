<?php

use Illuminate\Database\Migrations\Migration;

class CreateLogmessagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('logmessages', function($table)
		{
    		$table->increments('id');
    		$table->string('message');
    		$table->dateTime('time');
    		$table->string('sensor_type');
    		$table->integer('tasksession_id');
    		$table->foreign('tasksession_id')->references('id')->on('tasksessions')->onDelete('cascade');;
    		$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('logmessages');
	}

}