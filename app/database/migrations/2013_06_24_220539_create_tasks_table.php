<?php

use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tasks', function($table)
		{
    		$table->increments('id');
    		$table->string('name');
    		$table->string('description')->nullable();
    		$table->string('start_condition')->nullable();
    		$table->string('end_condition')->nullable();
    		$table->integer('project_id');
    		$table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');;
    		$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tasks');
	}

}