<?php

class Task extends Eloquent{

protected $guarded = array('id');

	public function tasksessions() {
		return $this->hasMany('Tasksession');
	}

    public function usersessions() {
        return $this->belongsToMany('Usersession', 'tasksessions', 'task_id','session_id');
    }
}