<?php

class Usersession extends Eloquent{

protected $guarded = array('id');

	public function tasksessions() {
		return $this->hasMany('Tasksession', 'session_id');
	}

	public function condition() {
		return $this->belongsTo('Condition');
	}

	public function user() {
		return $this->belongsTo('User');
	}

    public function tasks() {
        return $this->belongsToMany('Task', 'tasksessions','session_id', 'task_id');
    }
}