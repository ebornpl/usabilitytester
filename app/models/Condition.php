<?php

class Condition extends Eloquent{

protected $guarded = array('id');

    public function users()
    {
        return $this->belongsToMany('User','usersessions')->withPivot('id');
    }

    public function usersessions()
    {
        return $this->hasMany('Usersession');
    }
}