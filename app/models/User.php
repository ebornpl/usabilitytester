<?php

class User extends Eloquent{

protected $guarded = array('id');

    public function conditions()
    {
        return $this->belongsToMany('Condition','usersessions')->withPivot('id');
    }

    public function usersessions()
    {
        return $this->hasMany('Usersession');
    }

}