<?php

class Project extends Eloquent{

    protected $guarded = array('id');

    public function conditions()
    {
        return $this->hasMany('Condition');
    }

    public function tasks()
    {
        return $this->hasMany('Task')->orderBy('name');
    }

    public function users()
    {
        return $this->hasMany('User');
    }

}