<?php

class Tasksession extends Eloquent{

protected $guarded = array('id');

	public function task() {
		return $this->belongsTo('Task');
	}

    public function logmessages() {
        return $this->hasMany('Logmessage');
    }
}