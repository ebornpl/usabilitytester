<?php
/**
 * Created by IntelliJ IDEA.
 * User: eborn
 * Date: 04.07.2013
 * Time: 22:32
 * To change this template use File | Settings | File Templates.
 */

class Helpers
{
    const pageCondition = 0;
    const pageTask = 1;
    const pageUser = 2;
    const pageAssign = 3;
    const pageExecute = 4;

    public static function generateMenuLinksArray($project, $page)
    {
        $result = array(
            array(
                "name" => "Conditions",
                "action" => "ConditionController@index",
                "selected" => $page == Helpers::pageCondition
            ),
            array(
                "name" => "Tasks",
                "action" => "TaskController@index",
                "selected" => $page == Helpers::pageTask
            ),
            array(
                "name" => "Participants",
                "action" => "UserController@index",
                "selected" => $page == Helpers::pageUser
            ),
            array(
                "name" => "User assignment",
                "action" => "SessionController@index",
                "selected" => $page == Helpers::pageAssign
            ),
            array(
                "name" => "Test execution",
                "action" => "LogmessageController@index",
                "selected" => $page == Helpers::pageExecute
            )
        );
        if ($project["products"] == 1)
            unset($result[0]);
        if ($project["between_subject"] == 0)
            unset($result[3]);
        return $result;
    }

    public static function formatDuration($durationInSeconds)
    {

        $duration = '';
        $hours = floor($durationInSeconds / 3600);
        $durationInSeconds -= $hours * 3600;
        $minutes = floor($durationInSeconds / 60);
        $seconds = $durationInSeconds - $minutes * 60;

        $duration .= str_pad($hours, 2, "0", STR_PAD_LEFT) . ':';
        $duration .= str_pad($minutes, 2, "0", STR_PAD_LEFT) . ':';
        $duration .= str_pad($seconds, 2, "0", STR_PAD_LEFT);
        return $duration;
    }
}